package ru.korovyakov.edu.inheritance.entity;

import lombok.AccessLevel;
import lombok.Getter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
@Getter(AccessLevel.PROTECTED)
public abstract class Identifiable {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private int id;
}
