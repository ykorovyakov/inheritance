package ru.korovyakov.edu.inheritance.entity;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@DiscriminatorValue("DevId")
public class DeveloperSettings extends BaseSettings {
    @Column
    private String userData;

    @Column
    private String email;
}
