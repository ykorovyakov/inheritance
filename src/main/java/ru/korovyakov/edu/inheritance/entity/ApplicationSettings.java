package ru.korovyakov.edu.inheritance.entity;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@DiscriminatorValue("AppId")
public class ApplicationSettings extends BaseSettings {
    @Column
    private String os;

    @Column
    private String ram;
}
