package ru.korovyakov.edu.inheritance.entity;

import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@NoArgsConstructor
@Table(name = "settings")
@DiscriminatorColumn(name = "type_id", discriminatorType = DiscriminatorType.STRING)
public class BaseSettings extends Identifiable {
    @Column(insertable = false, updatable = false)
    private String type_id;
}
