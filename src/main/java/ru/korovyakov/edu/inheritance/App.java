package ru.korovyakov.edu.inheritance;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.korovyakov.edu.inheritance.config.MainConfig;

public class App {
    public static void main(String[] args) throws Exception {
        final ApplicationContext context = new AnnotationConfigApplicationContext(MainConfig.class);

        final InheritanceApp app = context.getBean(InheritanceApp.class);
        app.start();
    }
}
