package ru.korovyakov.edu.inheritance.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("ru.korovyakov.edu.inheritance")
public class MainConfig {
}
