package ru.korovyakov.edu.inheritance.repositories;

import org.springframework.stereotype.Repository;
import ru.korovyakov.edu.inheritance.api.SettingsRepository;
import ru.korovyakov.edu.inheritance.common.HibernateUtil;
import ru.korovyakov.edu.inheritance.entity.BaseSettings;

@Repository
public class SettingsRepositoryImpl implements SettingsRepository {
    @Override
    public void saveSettings(final BaseSettings settings) {
        HibernateUtil.getEm().getTransaction().begin();
        HibernateUtil.getEm().persist(settings);
        HibernateUtil.getEm().getTransaction().commit();
    }
}
