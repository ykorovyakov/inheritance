package ru.korovyakov.edu.inheritance.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.korovyakov.edu.inheritance.api.SettingsRepository;
import ru.korovyakov.edu.inheritance.api.SettingsService;
import ru.korovyakov.edu.inheritance.entity.ApplicationSettings;
import ru.korovyakov.edu.inheritance.entity.DeveloperSettings;

@Service
public class SettingsServiceImpl implements SettingsService {
    @Autowired
    private SettingsRepository repositories;

    @Override
    public void processSettings(final DeveloperSettings devSettings, final ApplicationSettings appSettings) {
        repositories.saveSettings(devSettings);
        repositories.saveSettings(appSettings);
    }
}
