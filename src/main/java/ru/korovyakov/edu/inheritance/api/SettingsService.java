package ru.korovyakov.edu.inheritance.api;

import ru.korovyakov.edu.inheritance.entity.ApplicationSettings;
import ru.korovyakov.edu.inheritance.entity.DeveloperSettings;

public interface SettingsService {
    void processSettings(DeveloperSettings devSettings, ApplicationSettings appSettings);
}
