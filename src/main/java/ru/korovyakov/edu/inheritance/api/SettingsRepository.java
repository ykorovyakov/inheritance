package ru.korovyakov.edu.inheritance.api;

import ru.korovyakov.edu.inheritance.entity.BaseSettings;

public interface SettingsRepository {
    void saveSettings(BaseSettings settings);
}
