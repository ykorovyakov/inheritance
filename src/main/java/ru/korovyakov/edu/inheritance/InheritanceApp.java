package ru.korovyakov.edu.inheritance;

import lombok.var;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.korovyakov.edu.inheritance.api.SettingsService;
import ru.korovyakov.edu.inheritance.entity.ApplicationSettings;
import ru.korovyakov.edu.inheritance.entity.DeveloperSettings;

import java.util.Scanner;

@Component
public class InheritanceApp {
    @Autowired
    private SettingsService service;

    public void start() {
        final var rawData = requestData();

        var dev = makeDevSettings(rawData);
        var app = makeAppSettings(rawData);

        service.processSettings(dev, app);
    }

    private DeveloperSettings makeDevSettings(final RawData rawData) {
        return new DeveloperSettings(rawData.userData, rawData.email);
    }

    private ApplicationSettings makeAppSettings(final RawData rawData) {
        return new ApplicationSettings(rawData.os, rawData.ram);
    }

    private RawData requestData() {
        final var rawData = new RawData();
        final Scanner scanner = new Scanner(System.in);

        System.out.print("Введите ФИО: ");
        rawData.userData = scanner.nextLine();

        System.out.print("Введите email: ");
        rawData.email = scanner.nextLine();

        System.out.print("Введите OC: ");
        rawData.os = scanner.nextLine();

        System.out.print("Введите объем памяти: ");
        rawData.ram = scanner.nextLine();

        return rawData;
    }
}
